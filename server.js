const express = require('express');
const app = express();

const getRandomInt = (min, max) => Math.floor(Math.random() * (max - min + 1) + min);

app.set('port', (process.env.PORT || 5001));

app.use(express.static(__dirname + '/public'));

app.get('/', (request, response) => {
    response.render('public/index');
});

app.get('/api/getGift', (request, response) => {
    const giftId = getRandomInt(1, 5);

    setTimeout(() => {
        response.json({
            'res': true,
            'giftId': giftId
        });
    }, 2 * 1000);
});

app.listen(app.get('port'), () => {
    console.log('Node app is running on port', app.get('port'));
});
