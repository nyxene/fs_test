const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');

module.exports = {

    entry: {
        main: './src/index.js'
    },

    resolve: {
        extensions: ['.js', '.json'],
        modules: [path.join(__dirname, 'src'), 'node_modules']
    },

    module: {
        rules: [
            {
                test: /\.scss$/,
                use: ExtractTextPlugin.extract({
                    use: [
                        { loader: 'css-loader' },
                        { loader: 'sass-loader' }
                    ],
                    fallback: 'style-loader'
                })
            },
            {
                test: /\.(jpg|png|gif)$/,
                use: 'file-loader'
            },
            {
                test: /\.(json)$/,
                use: ['json-loader']
            }
        ]
    },

    plugins: [

        new CleanWebpackPlugin(['public']),

        new HtmlWebpackPlugin({
            title: 'FS TEST',
            template: './src/index.html',
            chunksSortMode: 'dependency',
            minify: {
                collapseWhitespace: true,
                removeComments: true,
            }
        }),

        new ExtractTextPlugin({
            filename: (getPath) => {
                return getPath('[name].[hash].css');
            },
            allChunks: true
        }),

        new webpack.HashedModuleIdsPlugin(),
    ]
};
