const path = require('path');
const webpack = require('webpack');
const merge = require('webpack-merge');
const commonConfig = require('./webpack.common.js');

module.exports = merge(commonConfig, {

    devtool: 'eval',

    output: {
        path: path.join(__dirname, './public'),
        filename: '[name].[hash].js',
        publicPath: '/',
        sourceMapFilename: '[name].map'
    },

    devServer: {
        port: 3000,
        host: 'localhost',
        historyApiFallback: true,
        noInfo: false,
        contentBase: './dist',
        hot: true
    },

    plugins: [

        new webpack.optimize.CommonsChunkPlugin({
            name: 'runtime'
        }),

        new webpack.HotModuleReplacementPlugin()
    ]
});
