import './commons/styles/styles.scss';

import { GiftDialog } from './components/components';

const giftDialog = new GiftDialog(document.getElementById('gift-dialog'));

giftDialog.show();
