import 'whatwg-fetch';

import { API_BASE_URL } from './constants';

const headers = {
    'Content-Type': 'application/json; charset=utf-8',
    'Accept': 'application/json',
};

function checkStatus(resp) {
    if (resp.status >= 200 && resp.status < 300) {

        return resp;

    } else {
        const error = new Error(resp.statusText);
        error.response = resp;
        throw error;
    }
}

function parseJSON(response) {
    return response.json();
}

export const api = {

    get(url, params) {
        return fetch(`${API_BASE_URL}${url}`, { method: 'GET', headers, params })
            .then(checkStatus)
            .then(parseJSON)
            .catch(error => {
                console.error('request failed', error);
            });
    }
};
