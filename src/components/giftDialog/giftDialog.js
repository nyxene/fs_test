import { api } from '../../commons/api';
import './giftDialog.scss';

const VIEW = {
    0: {
        title: 'Поздравляем!',
        message: 'Вы победили там-то, и вам полагаеся какой-то подарок',
        buttonLabel: 'Получить',
    },
    1: {
        title: '',
        message: 'Теперь подарок ваш!',
        buttonLabel: 'Спасибо',
    },
};

export class GiftDialog {

    constructor(dialog) {
        this.body = document.body;
        this.dialog = dialog;
        this.isGivenGift = false;
        this.isPending = false;

        this.image = dialog.querySelector('.js-image');
        this.title = dialog.querySelector('.js-title');
        this.message = dialog.querySelector('.js-message');

        this.buttonOk = dialog.querySelector('.js-ok');
        this.buttonCancel = dialog.querySelector('.js-cancel');

        this._updatedView({ isGivenGift: false });
        this._lockButton(false);
    }

    /**
     * Открыть окно
     */
    show() {
        this.body.style = 'overflow: hidden';
        this.dialog.classList.add('is-visible');

        this.buttonOk.addEventListener('click', this._buttonHandler.bind(this));
        this.buttonCancel.addEventListener('click', this.close.bind(this));
    }

    /**
     * Закрыть окно
     */
    close() {
        this.body.style = 'overflow: auto';
        this.dialog.classList.remove('is-visible');

        this.buttonOk.removeEventListener('click', this._buttonHandler.bind(this));
        this.buttonCancel.removeEventListener('click', this.close.bind(this));
    }

    /**
     * Обработать клик по кнопке «Получить» или «Спасибо»
     */
    _buttonHandler() {

        if (this.isGivenGift) {

            this.close();
            return;
        }

        this._getGift();
    }

    /**
     * Получить подарок
     *
     * @private
     */
    _getGift() {

        if (this.isPending) {
            return;
        }

        this.isPending = true;
        this._lockButton(true);

        api.get('/getGift')
            .then(resp => {
                this.isGivenGift = true;
                this.isPending = false;

                this._lockButton(false);
                this._updatedView({ isGivenGift: true, isLockButton: false, imageId: resp.giftId });
            })
            .catch(err => {
                console.log(err);
            });
    }

    /**
     * Обновить информацию
     *
     * @param {boolean} isGivenGift - Флаг отображения подарка
     * @param {number|null} [imageId=null] - Id изображения
     * @private
     */
    _updatedView({ isGivenGift, imageId = null }) {

        const { title, message, buttonLabel } = VIEW[Number(isGivenGift)];

        this.title.innerText = title;
        this.message.innerText = message;
        this.buttonOk.innerText = buttonLabel;

        isGivenGift && this._addedImage(imageId);
    }

    /**
     * Заблокировать кнопку получения подарка
     *
     * @param {boolean} isLockButton - Флаг блокировки кнопки
     * @private
     */
    _lockButton(isLockButton) {
        isLockButton
            ? this.buttonOk.setAttribute('disabled', 'disabled')
            : this.buttonOk.removeAttribute('disabled');
    }

    /**
     * Добавить изображение
     *
     * @param {number} imageId - Id изображения
     * @private
     */
    _addedImage(imageId) {
        const image = new Image();
        image.src = require(`./images/${imageId}.png`);
        image.alt = 'Подарок';

        this.image.appendChild(image);
    }
}
