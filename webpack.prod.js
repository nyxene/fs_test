const path = require('path');
const webpack = require('webpack');
const merge = require('webpack-merge');
const commonConfig = require('./webpack.common.js');

module.exports = merge(commonConfig, {

    output: {
        path: path.join(__dirname, './public'),
        filename: '[name].[hash].js',
        publicPath: '/',
    },

    plugins: [

        new webpack.LoaderOptionsPlugin({
            minimize: true,
            debug: false
        }),

        new webpack.DefinePlugin({
            'process.env': {
                'NODE_ENV': JSON.stringify('production')
            }
        }),

        // new webpack.optimize.UglifyJsPlugin({
        //     beautify: false,
        //     mangle: true,
        //     comments: false
        // })
    ]
});
